The MAgnificently Designed Patchset:

A collection of UPK Patches and .ini edits to provide a b14-like 
experience in b15, while still trying to make the game feel balanced.

Tactical save games for too easy or too hard missions are appreciated.

DGC.ini changes:

* Removed the extra damage on Impossible
* Removed Covering Fire from Floater NAVIGATORS. Floater Leaders still have it.
* Covering Fire Floater Leaders have an extra point of health.
* Thin Men Navigators get a 25% chance at 90 Alien Research for Holo-Targeting
* Thin Men Navigators get a 25% chance at 270 Alien Reseach (rougly December year 1) for Shredder Ammo
* Level 7 Muton Leaders lose Tandem Warheads, get HEAT Ammo (they also have LEM, be careful!)
* Muton Navigators 650 Research chance for Tandem Warheads lowered to 20% (was 50%)
* Level 6 Sectoid Commander Leaders trade CCS for Shredder Ammo
* Level 7 Muton Elite leaders trade RTS for HEAT Ammo (gated at 450 research)
* Muton Elite Navigators have a 33% chance for Holo-Targeting
* Level 5 Outsider leaders trade CCS for Shredder Ammo
* Outsiders no longer get MFD, but I've allowed to climb ladders, I'm not sure if their AI will use ladders, though
* Outsiders no longer get Neural Damping, there's no reason for it at all
* ALL Sectopods get Advanced Fire Control
* Level 6 Sectopod Leaders get Absorption Fields (gated at 720 research, roughly year 2)
* Drone navigators get a 15% chance at 330 alien research (roughly February year 2) for Shredder Ammo